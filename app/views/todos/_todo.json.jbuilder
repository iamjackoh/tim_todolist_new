json.extract! todo, :id, :task, :date, :complete, :created_at, :updated_at
json.url todo_url(todo, format: :json)
