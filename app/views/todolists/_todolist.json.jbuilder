json.extract! todolist, :id, :task, :date, :complete, :created_at, :updated_at
json.url todolist_url(todolist, format: :json)
