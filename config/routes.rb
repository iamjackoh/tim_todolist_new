
Rails.application.routes.draw do
  get 'explore/pie'

  resources :todolists
  devise_for :users, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}
  resources :todos
  root to: "todos#index"
  get 'todos/index'
end