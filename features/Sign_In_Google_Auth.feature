Feature:	Sign In Using Google Authentication

			As a user,
			I want to sign in via Google Authentication
			so that I can create new Todos, and edit existing ones

Background:	Given I am on Log In Page

Scenario 1:	Display "Choose an account" page
			When I click on “Sign in with GoogleOauth2” button
			Then I am on "Choose an account page"

Scenario 2:	Display "Google Sign In page
			Given I am on Google Sign In Page
			When I click on "Use another account" button
			Then I am on "Google Sign In" page
		
Scenario 3:	Sign In to Web-App using Google Authentication
			Given I am on "Google Sign In" page
			When I click fill in the "Email" field with "shokha1@gmail.com"
			And I fill in the password with "Password"
			And I click on "Next"
			Then I am in Todos homepage
			And I can read a message on top of the page that reads “Successfully authenticated from Google account.”