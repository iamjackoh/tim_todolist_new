Feature:	Editing a Todo
			As a logged in user,
			I want to edit a Todo
			So that I can make changes and save it

Background:	Given I am signed in
			And I am on "Todos" homepage
	
Scenario 1:	Display the "Editing Todo" Page
			Given there is a Todo with Task: "Study", Date: "1", Complete: "True"
			When I click on "Edit" button next to it
			Then I am on "Editing Todo" page with Task: "Study", Date: "1", Complete: "True"
			
Scenario 2: Edit an existing Todo
			Given I am on "Editing Todo" page
			And the fields are filled with Task: "Study", Date: "1", Complete: "True"
			When I change Task field from "Study" to "Play Football"
			And I change Date field from "1" to "3"
			And I uncheck the Complete Box
			And I click on "Update Todo" button
			Then I am on a page that displays Task: "Play Football", Date: "3", Complete: "False"
			And I can read a message on top of the page "Todo was successfully updated."
			