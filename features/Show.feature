Feature:	Show a Todo
			As a logged in user,
			I want to view a Todo
			So that I can read the details of the Todoo
			
Background:	Given I am signed in
			And I am on "Todos" homepage
			And I can view a Task: "Study", Date: "1", Complete: "False"
	
Scenario:	Show a Todo
			When I click on "Show" Button
			Then I am on a page displaying "Task: Study", "Date: 1", "Complete: False"