Feature:	Creating a new todo
			As a logged in user,
			I want to create a new todo
			So that I can take a quick look at the todo later

Background:	Given I am signed in
			And I am on "Todos" homepage
	
Scenario 1:	Display "New Todo" Form upon clicking "New Todo"
			When I click on the "New Todo" button
			Then I am on "New Todo" page

Scenario 2:	Create a New Todo
			Given I am on "New Todo" page
			When I fill in the "Task" field with "Study"
			And I fill in the "Date" field with "1"
			And I check the "Complete" box
			And I click on the "Create Todo" button
			Then I am on "Todos" homepage
			And I can read a message on top of the page that reads "Todo was successfully created."

Scenario 3:	Creating a second Todo with the same details as one that already exists
			Given I am on "Todos" homepage
			And I can find a Todo with "Task: Study", "Date: 1", and "Complete: True"
			When I click on "New Todo" button 
			Then I am on "New Todo" page
			And I fill in the "Task" field with "Study"
			And I fill in the "Date" field with "1"
			And I check the "Complete" box
			And I click on the "Create Todo" button
			Then I am on "Todos" homepage
			And I can read a message on top of the page that reads "Todo was successfully created."
			