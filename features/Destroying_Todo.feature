Feature:	Destroying an existing todo
			As a logged in user,
			I want to destroy an existing todo
			So that only the undone todos are displayed

Background:	Given I am signed in
			And I am on "Todos" homepage
			
Scenario:	Given there is a "Task: Study" on "Todos" homepage
			When I click the "Destroy" button next to it
			Then a pop-up window opens 
			And a message on the pop-up window reads “Are you sure?”
			When I click on “OK” button
			Then I am on "Todos" homepage
			And I can read a message on top of the page "Todo was successfully destroyed."
